


import UIKit
import AVFoundation




class ResultRecargaViewController: UIViewController{
    
    
    //variables que recibimos de las clases pasadas
    
    var nombre = ""
    var fecha = ""
  var tarjeta = ""
    var monto = ""
    var mensaje = ""
    
    
    //si es rechazada o aceptada
    var status = false
    

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var cardLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    
    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var resultIcon: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.nameLabel.text = self.nombre
        self.dateLabel.text = self.fecha
        self.cardLabel.text = self.tarjeta
        self.amountLabel.text = "S/\(self.monto)"
  
    
        if self.status == true{
            //fue aprobada
            self.resultIcon.image = #imageLiteral(resourceName: "exitoIcon")
            self.resultLabel.text = self.mensaje
            
            
        }else{
            //fue rechazada
            
               self.resultIcon.image = #imageLiteral(resourceName: "rojoFallida")
             self.resultLabel.text = self.mensaje
        }
        
        
        
        
    }
    
  
    
    
    
    @IBAction func backAction(_ sender: UIButton) {
        
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: HomeViewController.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
        
    }
    
  
    
    
    //metodo que se ejecuta al presionar el botón de compartir
    @IBAction func shareAction(_ sender: UIButton) {
        
        
        DispatchQueue.main.async {
            
            UIGraphicsBeginImageContextWithOptions(self.view.bounds.size, false, 0);
            self.view.drawHierarchy(in: self.view.bounds, afterScreenUpdates: true)
            if let imageScreen = UIGraphicsGetImageFromCurrentImageContext(){
                
                UIGraphicsEndImageContext()
                let activityViewController = UIActivityViewController(activityItems: [imageScreen], applicationActivities: nil)
                
                activityViewController.excludedActivityTypes = [.addToReadingList,
                                                                .airDrop,
                                                                .assignToContact,
                                                                .copyToPasteboard,
                                                                .mail,
                                                                .message,
                                                                .print,
                                                                .saveToCameraRoll,
                                                                .postToWeibo,
                                                                .copyToPasteboard,
                                                                .saveToCameraRoll,
                                                                .postToFlickr,
                                                                .postToVimeo,
                                                                .postToTencentWeibo,
                                                                .postToFacebook,
                                                                .postToTwitter
                ]
                
                self.present(activityViewController, animated: true, completion: {})
            }
            
        }
        
        
    }
    
    
    
    }
    

   






