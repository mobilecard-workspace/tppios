


import UIKit
import AVFoundation




class ResultViewController: UIViewController{
    
    //variables que recibimos de las clases pasadas
    @IBOutlet weak var recargarButton: UIButton!
    @IBOutlet weak var recargarLabel: UILabel!
    @IBOutlet weak var nombreEtiquetaLabel: UILabel!
    
    @IBOutlet weak var tarjetaLabel: UILabel!
    @IBOutlet weak var fechaEtiquetaLabel: UILabel!
    var card : CardModel!
    var mensaje = ""
    
    //si es rechazada o aceptada
    var status = false
    
    @IBOutlet weak var centerContraint: NSLayoutConstraint!
    @IBOutlet weak var oldContraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var cardLabel: UILabel!
    
    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var resultIcon: UIImageView!

   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.nameLabel.text = self.card.nombre
        self.dateLabel.text = self.card.fechaAlta
        self.cardLabel.text = self.card.pan
        
        
        if self.status == true{
            //fue aprobada
            self.resultIcon.image = #imageLiteral(resourceName: "exitoIcon")
            self.resultLabel.text = self.mensaje
            
            
        }else{
            //fue rechazada
            self.recargarLabel.text = ""
           self.nombreEtiquetaLabel.text = ""
            self.fechaEtiquetaLabel.text = ""
            self.tarjetaLabel.text = ""
            self.recargarButton.isUserInteractionEnabled = false
            self.recargarButton.isHidden = true
            
            self.centerContraint.priority = UILayoutPriority(rawValue: 1000)
            self.oldContraint.priority = UILayoutPriority(rawValue: 900)
            
            self.resultIcon.image = #imageLiteral(resourceName: "rojoFallida")
            self.resultLabel.text = self.mensaje
        }
  
      
        
        
        
        
    }
    
  
    
    
    
    @IBAction func backAction(_ sender: UIButton) {
        
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: HomeViewController.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
        
    }
    
  
    
    @IBAction func reacargarAction(_ sender: UIButton) {
        
        
        
        let bundle  = Bundle(for: SelectCardViewController.self)
        let story = UIStoryboard(name: "teclado", bundle: bundle)
        
        let vc = story.instantiateViewController(withIdentifier: "tecladoIdentifier") as! TecladoViewController
        vc.card = self.card
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    
    //metodo que se ejecuta al presionar el botón de compartir
    @IBAction func shareAction(_ sender: UIButton) {
        
        
        DispatchQueue.main.async {
            
            UIGraphicsBeginImageContextWithOptions(self.view.bounds.size, false, 0);
            self.view.drawHierarchy(in: self.view.bounds, afterScreenUpdates: true)
            if let imageScreen = UIGraphicsGetImageFromCurrentImageContext(){
                
                UIGraphicsEndImageContext()
                let activityViewController = UIActivityViewController(activityItems: [imageScreen], applicationActivities: nil)
                
                activityViewController.excludedActivityTypes = [.addToReadingList,
                                                                .airDrop,
                                                                .assignToContact,
                                                                .copyToPasteboard,
                                                                .mail,
                                                                .message,
                                                                .print,
                                                                .saveToCameraRoll,
                                                                .postToWeibo,
                                                                .copyToPasteboard,
                                                                .saveToCameraRoll,
                                                                .postToFlickr,
                                                                .postToVimeo,
                                                                .postToTencentWeibo,
                                                                .postToFacebook,
                                                                .postToTwitter
                ]
                
                self.present(activityViewController, animated: true, completion: {})
            }
            
        }
        
        
    }
    
    }
    

   






