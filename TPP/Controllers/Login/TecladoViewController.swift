


import UIKit
import AVFoundation
import Alamofire




class TecladoViewController:UIViewController,ServicesDelegate{

    
    

    
    //referencias de elementos graficos
    @IBOutlet weak var oneButton: UIButton!
    @IBOutlet weak var twoButton: UIButton!
    @IBOutlet weak var threeButton: UIButton!
    @IBOutlet weak var fourButton: UIButton!
    @IBOutlet weak var fiveButton: UIButton!
    @IBOutlet weak var sixButton: UIButton!
    @IBOutlet weak var sevenButton: UIButton!
    @IBOutlet weak var eightButton: UIButton!
    @IBOutlet weak var nineButton: UIButton!
    @IBOutlet weak var ceroButton: UIButton!
    @IBOutlet weak var eraseButton: UIButton!
    
    @IBOutlet weak var conceptButton: UIButton!
    
    @IBOutlet weak var totalAmountLabel: UILabel!
    
   
    
    var amountGlobal = "0.0"
    

    var amountCard = "0.0"
  
    //objeto que recibimos de la clase pasada
    var card : CardModel!
    
    //objeto que gestiona los servicios
    let service = Services()
    
    // formato para los botones
    var attrs = [
        NSAttributedString.Key.font : UIFont.systemFont(ofSize: 24.0),
        NSAttributedString.Key.foregroundColor : UIColor.black,
        NSAttributedString.Key.underlineStyle : 1] as [NSAttributedString.Key : Any]
    
    var attributedString = NSMutableAttributedString(string:"")
    
    var valueString = "S/"
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        service.delegate = self
       
       
      
        
        
        
        
        //agregamos los underline de los botones
        self.attrs[NSAttributedString.Key.font] = self.oneButton.titleLabel?.font
        self.underlineButton(button: oneButton)
        self.underlineButton(button: twoButton)
        self.underlineButton(button: threeButton)
        self.underlineButton(button: fourButton)
        self.underlineButton(button: fiveButton)
        self.underlineButton(button: sixButton)
        self.underlineButton(button: sevenButton)
        self.underlineButton(button: eightButton)
        self.underlineButton(button: nineButton)
        self.underlineButton(button: ceroButton)
        
        
   
        
        


        
     
        
      
        
        
       

        
        
    }
    
  
    
 
    
    override func viewWillAppear(_ animated: Bool) {
        

        
    }
    

    //metodo que le pone una underline a un botón
    func underlineButton(button:UIButton){
        
      self.attributedString =  NSMutableAttributedString(string:"")
        
        let buttonTitleStr = NSMutableAttributedString(string:button.titleLabel!.text!, attributes:attrs)
        attributedString.append(buttonTitleStr)
       
        button.setAttributedTitle(attributedString, for: .normal)
        
    }
    
  
    
    
    
    @IBAction func backAction(_ sender: UIButton) {
         _ = self.navigationController?.popViewController(animated: true)
    }
    
   
    //metodo que se ejecuta al presionar algun numero
    @IBAction func numberAction(_ sender: UIButton) {
        
        
        //opcion de borrado
        if sender.tag == 10{
            
            if self.totalAmountLabel.text != "S/"{
                var characters = Array(self.totalAmountLabel.text!)
                 characters.remove(at: characters.count-1)
                if characters.count > 1{
                   // self.totalAmountLabel.text = "S/"
                   // self.valueString = "S/"
                  //  return;
                //}else{
                var stringNew = ""
                var stringNoPunto = ""
                for char in characters{
                    if char != "."{
                        stringNoPunto .append(char)
                    }
                    stringNew.append(char)
                }
              //  self.totalAmountLabel.text = stringNew
                    var amount = ""
                    let arrayCharacters = Array(stringNew)
                    
                    for position in 2...arrayCharacters.count-1{
                        amount.append(arrayCharacters[position])
                        
                    }
                    
                    var amountTwo = ""
                    let arrayCharactersTwo = Array(stringNoPunto)
                    
                    for position in 2...arrayCharactersTwo.count-1{
                        amountTwo.append(arrayCharactersTwo[position])
                        
                    }
                    
                    print("esto vale amounttwo \(amountTwo)")
                    if amountTwo == "00"{
                        self.totalAmountLabel.text = "S/"
                        self.valueString = "S/"
                        return;
                        
                    }
                    
                    let doubleStr = String(format: "%.2f", Double(amountTwo)! / 100)
                    self.amountGlobal = doubleStr
                    self.totalAmountLabel.text = "S/\(self.amountGlobal)"
                    
                    var array = Array(self.valueString)
                    array.remove(at: array.count-1)
                    var stringComplete = ""
                    for a in array {
                        stringComplete.append(a)
                    }
                    self.valueString = stringComplete
                
                  
                return;
                    
                }
                
            }else{
                  self.valueString = "S/"
                self.alert(title: "¡Aviso!", message: "No hay valores que borrar actualmente.", cancel: "OK")
                return;
                
            }
            
            
            
        }
        //termina codigo de opcion de borrado
        
        
        
        //inicia codigo para numeros del 0 al 9
        if self.totalAmountLabel.text == "S/"{
            
            if sender.tag == 0{
                
                self.alert(title: "¡Aviso!", message: "Agrega un valor valido.", cancel: "OK")
               return;
            }else{
                
                
                self.valueString.append(String(sender.tag))
               
                
                var amount = ""
                let arrayCharacters = Array(self.valueString)
                
                for position in 2...arrayCharacters.count-1{
                    amount.append(arrayCharacters[position])
                    
                }
                
                let doubleStr = String(format: "%.2f", Double(amount)! / 100)
                self.amountGlobal = doubleStr
                self.totalAmountLabel.text = "S/\(self.amountGlobal)"
                
                
               
            
            }
            
            
            
            
        }else{
            
            self.valueString.append(String(sender.tag))
            var amount = ""
            let arrayCharacters = Array(self.valueString)
            
            for position in 2...arrayCharacters.count-1{
                amount.append(arrayCharacters[position])
                
            }
            
            
            let doubleStr = String(format: "%.2f", Double(amount)! / 100)
            self.amountGlobal = doubleStr
            
            self.totalAmountLabel.text = "S/\(self.amountGlobal)"
           // self.totalAmountLabel.text = valueString
        
            
        }
        
         //termina codigo para numeros del 0 al 9
        
        
    }
    
    
    //metodo que se ejecuta al presionar el botón de recargar
    @IBAction func reloadAction(_ sender: UIButton) {
        
        
        //requerimientos para avanzar en la vista
        if self.totalAmountLabel.text == "S/" || self.totalAmountLabel.text == "S/0.00"{
            self.alert(title: "¡Aviso!", message: "Debes agregar un monto para continuar.", cancel: "OK")
            return;
        }
        
        
     
            
        
        
        //obtenemos la cantidad a recargar en String
        var amount = ""
        let arrayCharacters = Array(self.totalAmountLabel.text!)
        
        for position in 2...arrayCharacters.count-1{
            amount.append(arrayCharacters[position])
            
        }
        
        
        
       
        
        self.amountCard = amount
            
        
        
        
        print("esto es el amountcard \("\(self.amountCard)")")
        let params  = ["idOperador":DataUser.USER_ID!,"idTarjeta":self.card.id,"monto":self.amountCard]
        
        
        
        let headers  : HTTPHeaders = ["Content-Type":"application/json","Authorization":"Basic TWIxbDNDNHJkbVg6MUxvMGZRI3hHVHN1MXojNFs0TCo="]
        
        
        self.service.send(type: .POST, url: "TPP/5/4/es/operador/recarga", params: params as [String : Any], header: headers, message: "Recargando",animate: false)
        
            
            //aqui
        //llamamos a la vista
       // self.performSegue(withIdentifier: "PECardPressentStep2", sender: nil)
            
            
            
    
    }

    
    
  
    
   
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        //acción antes del segue
        if segue.identifier == "PECardPressentStep2"{
            
        //    let destinoViewController = segue.destination as! PECardPressentStep2CommerceViewController
            
            //destinoViewController.cardPressent = self.cardPressent
            
            
            
        }
        
        
      
    }
 
   
    

    
    
    
    
   
    
    
    //metodos delegado servicios
    
    func responseService(type: ServicesTypes, endpoint: String, response: NSDictionary) {
        
        print("RESPONSE: \(response)")
        
        
        if endpoint == "TPP/5/4/es/operador/recarga"{
          
            
            let idError = response["idError"] as! Int
            
            if idError == 0{
                
                let mensajeError = response["mensajeError"] as! String
            
                let fecha = response["fecha"] as! String
                let vc = ResultRecargaViewController()
                vc.nombre = self.card.nombre
                vc.tarjeta = self.card.pan
                vc.fecha = fecha
                vc.monto = self.amountCard
                vc.status = true
                vc.mensaje = mensajeError
                self.navigationController?.pushViewController(vc, animated: true)
            
            }else{
                //error
                   let mensajeError = response["mensajeError"] as! String
                let fecha = response["fecha"] as! String
                let vc = ResultRecargaViewController()
                vc.nombre = self.card.nombre
                vc.tarjeta = self.card.pan
                vc.fecha = fecha
                vc.monto = self.amountCard
                vc.status = false
                vc.mensaje = mensajeError
                self.navigationController?.pushViewController(vc, animated: true)
                
                
                
            }
            
        }
        
    }
    
    func responseArrayService(type: ServicesTypes, endpoint: String, response: NSArray) {
        
    }

    func errorService(endpoint: String) {
        
        
            
           self.alert(title: "¡Aviso!", message: "No fue posible establecer conexión con el servidor.", cancel: "OK")
            
        
        
    }
    
    
    
   
}








