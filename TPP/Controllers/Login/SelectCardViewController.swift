


import UIKit
import AVFoundation
import Alamofire



class SelectCardViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,ServicesDelegate,UITextFieldDelegate{
    
  
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchTextField: UITextField!
    
    //tableview nibs
    let nibTable: String = "CardTableViewCell"
    let cellTableIdentifier: String = "cardIdentifier"
  
    var busqueda = ""
    
    @IBOutlet weak var menuView: UIView!

    
    //arreglo que controla la tableview
    var data : [CardModel] = []
    
    let services = Services()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.searchTextField.delegate = self
        self.services.delegate = self
        
        
  
        //registro nibs
        self.tableView.register(UINib(nibName: self.nibTable, bundle: nil), forCellReuseIdentifier: self.cellTableIdentifier)
 
        
        
     
        
        
        
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        
        
        let str = self.searchTextField.text!
        //cambiamos los espacios
         self.busqueda = str.replacingOccurrences(of: " ", with: "%20")
        
        //quitamos los acentos
        self.busqueda = self.busqueda.folding(options: .diacriticInsensitive, locale: .current)
       
        let params : [String:Any] = [:]
        
        
        let headers  : HTTPHeaders = ["Content-Type":"application/json","Authorization":"Basic TWIxbDNDNHJkbVg6MUxvMGZRI3hHVHN1MXojNFs0TCo="]
        print("si entro aqui")
        
        
        
        self.services.send(type: .GET, url: "TPP/5/4/es/operador/\(DataUser.USER_ID!)/getCards?nombre=\(self.busqueda)", params: params, header: headers, message: "Obteniendo",animate: false)
        
        
        
    }
    
 
  
    
    
    @IBAction func backAction(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    

    
    
    
    //metodos datasource de la tableview
    
    
    //metodos data source de tableview
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
       
    return self.data.count == 0 ? 1 : self.data.count
    
        
        
    }
    
    
    
    
 
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if (self.data.count == 0) {
            let cell:UITableViewCell = UITableViewCell(style: .default, reuseIdentifier: "No");
            cell.textLabel?.text = "Sin tarjetas disponibles";
            cell.selectionStyle = .none
            return cell;
        }
        
       
        
            let cell: CardTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: self.cellTableIdentifier) as! CardTableViewCell
        
        cell.nameLabel.text = self.data[indexPath.row].nombre
        cell.cardLabel.text = self.data[indexPath.row].pan
        
        
     return cell
            
        
            
    
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
   
        
    
        return 110
   
        
    }
    
 
    
    //metodos delegado servicios
    func responseService(type: ServicesTypes, endpoint: String, response: NSDictionary) {
        
        print(response)
        
        
        if endpoint == "TPP/5/4/es/operador/\(DataUser.USER_ID!)/getCards?nombre=\(self.busqueda)"{
            
            let idError = response["idError"] as! Int
            
            if idError == 0{
                
                
       
               
                let indexPath = IndexPath(row: 0, section: 0)
               self.tableView.scrollToRow(at: indexPath, at: .top, animated: true)
              
                self.data = []
                
                let dataArray = response["cards"] as! NSArray
                
                for element in dataArray{
                    
                    let dictio = element as! NSDictionary
                    
                    let fechaAlta = dictio["fechaAlta"] as! String
                    let nombre = dictio["nombre"] as! String
                    let pan = dictio["pan"] as! String
                    let id = String(dictio["id"] as! Int)
                     let decrypter = pootEngine()
                    let panDecript = decrypter.mcDecryptString(pan, withSensitive: false)
                    
                   let arrayChars = Array(panDecript!)
                    
                    let panFinish = "\(arrayChars[0])\(arrayChars[1])\(arrayChars[2])\(arrayChars[3])-****-****-\(arrayChars[arrayChars.count-4])\(arrayChars[arrayChars.count-3])\(arrayChars[arrayChars.count-2])\(arrayChars[arrayChars.count-1])"
                    
                    let objectTemp = CardModel(id: id, nombre: nombre, pan: panFinish, fechaAlta: fechaAlta)
                    
                    self.data.append(objectTemp)
                    
                }
                self.tableView.reloadData()
                
            }else{
                let mensaje = response["mensajeError"] as! String
                
                self.alert(title: "¡Aviso!", message: mensaje, cancel: "OK")
                
            }
        }
        
        
      
        
    }
    
    
    func responseArrayService(type: ServicesTypes, endpoint: String, response: NSArray) {
        
    }
    
    func errorService(endpoint: String) {
        self.alert(title: "¡Aviso!", message: "No fue posible establecer conexión con el servidor.", cancel: "OK")
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
        if self.data.count > 0 {
        let bundle  = Bundle(for: SelectCardViewController.self)
        let story = UIStoryboard(name: "teclado", bundle: bundle)
        
        let vc = story.instantiateViewController(withIdentifier: "tecladoIdentifier") as! TecladoViewController
        vc.card = self.data[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
            
        }
        
    }
   
    
    
    

   
}







