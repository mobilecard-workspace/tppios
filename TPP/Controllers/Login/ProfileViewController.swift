


import UIKit
import AVFoundation
import Alamofire



class ProfileViewController: UIViewController,ServicesDelegate{
    
  
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var saldoLabel: UILabel!
    
    
    @IBOutlet weak var menuView: UIView!

    
    //arreglo que controla la tableview
    var data : [CardModel] = []
    
    let services = Services()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.services.delegate = self
        
        
  self.nameLabel.text = "\(DataUser.NAME!)"
 
        
        
        let params : [String:Any] = [:]
        
        
        let headers  : HTTPHeaders = ["Content-Type":"application/json","Authorization":"Basic TWIxbDNDNHJkbVg6MUxvMGZRI3hHVHN1MXojNFs0TCo="]
        
        
        self.services.send(type: .GET, url: "TPP/5/4/es/operador/\(DataUser.USER_ID!)/perfil", params: params, header: headers, message: "Obteniendo",animate: false)
        
        
        
    }
    
  
    
    
    @IBAction func backAction(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    

    

   
    
    
  
    
    
    //metodos delegado servicios
    func responseService(type: ServicesTypes, endpoint: String, response: NSDictionary) {
        
        print(response)
        
        
        if endpoint == "TPP/5/4/es/operador/\(DataUser.USER_ID!)/perfil"{
            
            let idError = response["idError"] as! Int
            
            if idError == 0{
                
                let nombre = response["nombre"] as! String
                self.nameLabel.text = nombre
                
                
                let card = response["card"] as! NSDictionary
                
                let saldo = String(card["saldo"] as! Double)
                
                self.saldoLabel.text = "S/\(saldo)"
               
              
              
                
            }else{
                let mensaje = response["mensajeError"] as! String
                
                self.alert(title: "¡Aviso!", message: mensaje, cancel: "OK")
                
            }
        }
        
        
      
        
    }
    
    
    func responseArrayService(type: ServicesTypes, endpoint: String, response: NSArray) {
        
    }
    
    func errorService(endpoint: String) {
        self.alert(title: "¡Aviso!", message: "No fue posible establecer conexión con el servidor.", cancel: "OK")
    }
    
    
   
    
    
    

   
}







