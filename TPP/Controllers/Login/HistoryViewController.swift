


import UIKit
import AVFoundation
import Alamofire



class HistoryViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,ServicesDelegate{
    
  
    @IBOutlet weak var tableView: UITableView!
    
    //tableview nibs
    let nibTable: String = "HistoryTableViewCell"
    let cellTableIdentifier: String = "historyIdentifier"
    let nibTableTwo: String = "RecargasTableViewCell"
    let cellTableIdentifierTwo: String = "recargasIdentifier"
    @IBOutlet weak var altasButton: UIButton!
    @IBOutlet weak var recargasButton: UIButton!
    
    @IBOutlet weak var menuView: UIView!
    @IBOutlet weak var altasRecargasLabel: UILabel!
    
    //arreglo que controla la tableview
    var data : [HistoryModel] = []
    
    let services = Services()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.services.delegate = self
        
        
  
        //registro nibs
        self.tableView.register(UINib(nibName: self.nibTable, bundle: nil), forCellReuseIdentifier: self.cellTableIdentifier)
           self.tableView.register(UINib(nibName: self.nibTableTwo, bundle: nil), forCellReuseIdentifier: self.cellTableIdentifierTwo)
        
        
        let params : [String:Any] = [:]
        
        
        let headers  : HTTPHeaders = ["Content-Type":"application/json","Authorization":"Basic TWIxbDNDNHJkbVg6MUxvMGZRI3hHVHN1MXojNFs0TCo="]
        
        
        self.services.send(type: .GET, url: "TPP/5/4/es/operador/\(DataUser.USER_ID!)/historial/1", params: params, header: headers, message: "Obteniendo",animate: false)
        
        
        
    }
    
  
    
    
    @IBAction func backAction(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    

    
    
    
    //metodos datasource de la tableview
    
    
    //metodos data source de tableview
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
       
    return self.data.count == 0 ? 1 : self.data.count
    
        
        
    }
    
    
    
    
 
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if (self.data.count == 0) {
            let cell:UITableViewCell = UITableViewCell(style: .default, reuseIdentifier: "No");
            cell.textLabel?.text = "Sin historicos disponibles";
            cell.selectionStyle = .none
            return cell;
        }
        
        if self.data[indexPath.row].type == "alta"{
        
            let cell: HistoryTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: self.cellTableIdentifier) as! HistoryTableViewCell
        
        cell.nameLabel.text = self.data[indexPath.row].nombre!
        cell.dateLabel.text = self.data[indexPath.row].fecha!
        cell.cardLabel.text = self.data[indexPath.row].tarjeta!
        
        
     return cell
            
            
        }else{
            
            let cell: RecargasTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: self.cellTableIdentifierTwo) as! RecargasTableViewCell
            
            cell.nameLabel.text = self.data[indexPath.row].nombre!
            cell.dateLabel.text = self.data[indexPath.row].fecha!
            cell.cardLabel.text = self.data[indexPath.row].tarjeta!
            cell.amountLabel.text = "S/\(self.data[indexPath.row].monto!)"
            
            return cell
            
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if self.data.count > 0{
        
        if self.data[indexPath.row].type == "alta"{
        
        return 120
            
        }else{
            
           return 145
        }
        
     
        }else{
            return 120
        }
        
    }
    
 
    
    //metodos delegado servicios
    func responseService(type: ServicesTypes, endpoint: String, response: NSDictionary) {
        
        print(response)
        
        
        if endpoint == "TPP/5/4/es/operador/\(DataUser.USER_ID!)/historial/1"{
            
            let idError = response["idError"] as! Int
            
            if idError == 0{
                
                
               self.altasRecargasLabel.text = "ALTAS"
               
             
                self.altasButton.backgroundColor = #colorLiteral(red: 0.9254901961, green: 0.4196078431, blue: 0, alpha: 1)
                self.recargasButton.backgroundColor = #colorLiteral(red: 0.6962813735, green: 0.6761380434, blue: 0.7027263641, alpha: 1)
                self.data = []
                
                let dataArray = response["data"] as! NSArray
                
                for element in dataArray{
                    
                    let dictio = element as! NSDictionary
                    
                    let fechaAlta = dictio["fechaAlta"] as! String
                    let nombre = dictio["nombre"] as! String
                    let pan = dictio["pan"] as! String
                     let decrypter = pootEngine()
                    let panDecript = decrypter.mcDecryptString(pan, withSensitive: false)
                    
                   let arrayChars = Array(panDecript!)
                    
                    let panFinish = "\(arrayChars[0])\(arrayChars[1])\(arrayChars[2])\(arrayChars[3])-****-****-\(arrayChars[arrayChars.count-4])\(arrayChars[arrayChars.count-3])\(arrayChars[arrayChars.count-2])\(arrayChars[arrayChars.count-1])"
                    
                    let objectTemp = HistoryModel(nombre: nombre, fecha: fechaAlta, tarjeta: panFinish, monto: "", type: "alta")
                    
                    self.data.append(objectTemp)
                    
                }
                self.tableView.reloadData()
                let indexPath = IndexPath(row: 0, section: 0)
                self.tableView.scrollToRow(at: indexPath, at: .top, animated: true)
            }
        }
        
        
        
        if endpoint == "TPP/5/4/es/operador/\(DataUser.USER_ID!)/historial/2"{
            
            let idError = response["idError"] as! Int
            
            if idError == 0{
                
                
                  self.altasRecargasLabel.text = "RECARGAS"
               
              
                self.altasButton.backgroundColor = #colorLiteral(red: 0.6962813735, green: 0.6761380434, blue: 0.7027263641, alpha: 1)
                self.recargasButton.backgroundColor = #colorLiteral(red: 0.9254901961, green: 0.4196078431, blue: 0, alpha: 1)
                self.data = []
                
                let dataArray = response["data"] as! NSArray
                
                for element in dataArray{
                    
                    let dictio = element as! NSDictionary
                    
                    let fechaAlta = dictio["fecha"] as! String
                    let nombre = dictio["nombre"] as! String
                    
                    let pan = dictio["tarjeta"] as! String
                    let decrypter = pootEngine()
                    let panDecript = decrypter.mcDecryptString(pan, withSensitive: false)
                    let monto = String(dictio["monto"] as! Double)
                    print("este es el panDecript \(panDecript)")
                    let arrayChars = Array(panDecript!)
                    
                    let panFinish = "\(arrayChars[0])\(arrayChars[1])\(arrayChars[2])\(arrayChars[3])-****-****-\(arrayChars[arrayChars.count-4])\(arrayChars[arrayChars.count-3])\(arrayChars[arrayChars.count-2])\(arrayChars[arrayChars.count-1])"
                    
                    let objectTemp = HistoryModel(nombre: nombre, fecha: fechaAlta, tarjeta: panFinish, monto: monto, type: "recarga")
                    
                    self.data.append(objectTemp)
                    
                }
                self.tableView.reloadData()
                
                let indexPath = IndexPath(row: 0, section: 0)
                self.tableView.scrollToRow(at: indexPath, at: .top, animated: true)
            }
        }
        
    }
    
    
    func responseArrayService(type: ServicesTypes, endpoint: String, response: NSArray) {
        
    }
    
    func errorService(endpoint: String) {
        self.alert(title: "¡Aviso!", message: "No fue posible establecer conexión con el servidor.", cancel: "OK")
    }
    
    
    //metodo que cambia de alta y recargas tag 0: alta tag 1: recargas
    @IBAction func changeSelectionAction(_ sender: UIButton) {
        
        
        if sender.tag == 0{
            //alta
            
            let params : [String:Any] = [:]
            
            
            let headers  : HTTPHeaders = ["Content-Type":"application/json","Authorization":"Basic TWIxbDNDNHJkbVg6MUxvMGZRI3hHVHN1MXojNFs0TCo="]
            
            
            self.services.send(type: .GET, url: "TPP/5/4/es/operador/\(DataUser.USER_ID!)/historial/1", params: params, header: headers, message: "Obteniendo",animate: false)
            
        }else{
            //recargas
            
            let params : [String:Any] = [:]
            
            
            let headers  : HTTPHeaders = ["Content-Type":"application/json","Authorization":"Basic TWIxbDNDNHJkbVg6MUxvMGZRI3hHVHN1MXojNFs0TCo="]
            
            
            self.services.send(type: .GET, url: "TPP/5/4/es/operador/\(DataUser.USER_ID!)/historial/2", params: params, header: headers, message: "Obteniendo",animate: false)
            
            
        }
        
    }
    
    
    
    
  
    

   
}







