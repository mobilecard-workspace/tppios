


import UIKit
import AVFoundation
import Alamofire
import CommonCrypto


class LoginViewController: UIViewController,ServicesDelegate{
  
  
    

    @IBOutlet weak var mailTextField: UITextField!
    
    @IBOutlet weak var passTextField: UITextField!
    
    let services = Services()
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
  self.services.delegate = self
      
        
        
        
        
    }
    
  
    
    
    

    
    
    //metodo que se ejecuta al presionar el botón de iniciar sesion
    @IBAction func startSesionAction(_ sender: UIButton) {
        
        if (self.mailTextField.text?.count)! < 4 {
            alert(title: "¡Aviso!", message: "Agrega un correo electrónico valido para continuar.", cancel: "OK")
            return;
        }
        
        
        if (self.passTextField.text?.count)! < 8 || (self.passTextField.text?.count)! > 12{
            alert(title: "¡Aviso!", message: "Agrega una contraseña valida para continuar '8-12 caracteres'.", cancel: "OK")
            return;
        }
        
        
        
        
        let pass = self.passTextField.text!.sha512()
        
        

        let params  = ["usuario":self.mailTextField.text!,"password":pass]
        
       
        let headers  : HTTPHeaders = ["Content-Type":"application/json","Authorization":"Basic TWIxbDNDNHJkbVg6MUxvMGZRI3hHVHN1MXojNFs0TCo="]

        print("PARAMS:\(params)")
        
        self.services.send(type: .POST, url: "TPP/5/4/es/operador/login", params: params, header: headers, message: "Ingresando",animate: false)
        
        
  
    
        
    
      
   
       
        
    }
    
    
  
    
    func responseService(type: ServicesTypes, endpoint: String, response: NSDictionary) {
        
        
           if endpoint == "TPP/5/4/es/operador/login"{
            
            let idError = response["idError"] as! Int
            
            if idError == 0{
                
                let id_user = String(response["id"] as! Int)
                let nombre = response["nombre"] as! String
                let card = response["card"] as! NSDictionary
                let monto = String(card["saldo"] as! Double)
                
                DataUser.USER_ID = id_user
                DataUser.NAME = nombre
                DataUser.SALDO = monto
        
        let vc = HomeViewController()
        self.navigationController?.pushViewController(vc, animated: true)
            
           }else{
            //fracaso
            
            let mensaje = response["mensajeError"] as! String
            self.alert(title: "¡Aviso!", message: mensaje, cancel: "OK")
            
        }
        
        
        }
        
    }
    
    func responseArrayService(type: ServicesTypes, endpoint: String, response: NSArray) {
        
    }
    
    func errorService(endpoint: String) {
        
        self.alert(title: "¡Aviso!", message: "No fue posible establecer conexión con el servidor.", cancel: "OK")
        
    }
    
    
 
    func sha512Base64(string: String) -> String {
        let digest = NSMutableData(length: Int(CC_SHA512_DIGEST_LENGTH))!
        if let data = string.data(using: String.Encoding.utf8) {

            let value =  data as NSData
            let uint8Pointer = UnsafeMutablePointer<UInt8>.allocate(capacity: digest.length)
            CC_SHA512(value.bytes, CC_LONG(data.count), uint8Pointer)

        }
        return digest.base64EncodedString(options: NSData.Base64EncodingOptions([]))
    }

   
}







//extension para mandar mensaje sin tanto codigo
extension UIViewController {
    func alert(title:String, message:String, cancel:String) {
        
        let alert:UIAlertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert);
        let action:UIAlertAction    = UIAlertAction(title: cancel, style: UIAlertAction.Style.cancel, handler: nil);
        
        alert.addAction(action);
        present(alert, animated: true, completion: nil);
        
        
        
    }
    

  
}

extension String {
    func sha512() -> String {
        let data = self.data(using: String.Encoding.utf8)!
        var digest = [UInt8](repeating: 0, count:Int(CC_SHA512_DIGEST_LENGTH))
      
        data.withUnsafeBytes {
            _ = CC_SHA512($0, CC_LONG(data.count), &digest)
            
        }
        return Data(digest).base64EncodedString()
    }
    func base64Encoded() -> String {
        if let data = self.data(using: .utf8) {
            return data.base64EncodedString()
        }
        return ""
    }
    
    
    

 
}

