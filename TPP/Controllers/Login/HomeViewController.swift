


import UIKit
import AVFoundation




class HomeViewController: UIViewController{
    
  
    

    @IBOutlet weak var mailTextField: UITextField!
    
    @IBOutlet weak var passTextField: UITextField!
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
  
      
        
        
        
        
    }
    
  
    
    
    //metodo que se ejecuta al presionar cualquier opcion del menu
    @IBAction func menuAction(_ sender: UIButton) {
        
        switch sender.tag{
            
        case 0 :
            let vc = EnrolarViewController()
            self.navigationController?.pushViewController(vc, animated: true)
            break
            
        case 1:
            let vc = SelectCardViewController()
            self.navigationController?.pushViewController(vc, animated: true)
            break
            
        case 2:
          let vc = HistoryViewController()
          self.navigationController?.pushViewController(vc, animated: true)
            
            break
            
        case 3:
            let vc = ProfileViewController()
            self.navigationController?.pushViewController(vc, animated: true)
            break
            
        default:
            break
            
            
            
        }
        
        
    }
    
    
    
  
    
    
    @IBAction func closeSesionAction(_ sender: UIButton) {
        
        
        let alert = UIAlertController(title: "Cerrar Sesión", message: "¿Quieres cerrar tu sesión?", preferredStyle: .alert);
        alert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil));
        alert.addAction(UIAlertAction(title: "Si", style: .default, handler: { (action:UIAlertAction) in
            
            //borramos los datos del usuario actuales
            DataUser.clean()
            
            
          
            
            let startViewController: LoginViewController  = LoginViewController()
            self.navigationController?.setViewControllers([startViewController,self], animated: false)
            self.navigationController?.setNavigationBarHidden(true, animated: false)
            
            let vc = LoginViewController()
            
            self.navigationController?.pushViewController(vc, animated: false)
            
   
            
        }));
        
        present(alert, animated: true, completion: nil);
        
    }
    
    
    
    @IBAction func supportAction(_ sender: UIButton) {
        
        
        let alert = UIAlertController(title: "Soporte", message: "¿Deseas llamar a soporte?", preferredStyle: .alert);
        alert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil));
        alert.addAction(UIAlertAction(title: "Si", style: .default, handler: { (action:UIAlertAction) in
            
            
            guard let number = URL(string: "telprompt://080080102") else { return }
            UIApplication.shared.open(number)
            
         
          
            
            
            
        }));
        
        present(alert, animated: true, completion: nil);
        
        
        
    }
    
    
    
    
    
    }
    

   






