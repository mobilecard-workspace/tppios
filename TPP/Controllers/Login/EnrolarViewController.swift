


import UIKit
import AVFoundation
import ActionSheetPicker_3_0
import Alamofire
import M13Checkbox


class EnrolarViewController: UIViewController,ServicesDelegate{
    
    //para la fecha
    var dayDatePicker: UIDatePicker!
    @IBOutlet weak var dayTextField: UITextField!
    
    var generoNombres : [String] = ["Masculino","Femenino"]
    var generoIds : [String] = ["M","F"]
    
    var generoSelected = ""
    
    //variables que guardan el codigo seleccionado
    var departamentCode = ""
    var provinceCode = ""
    var distritCode = ""
    
    //arreglos que contienen la informacion de provinsias departamentos distritos
    
    var departamentNames : [String] = []
    var departamentCodes : [String] = []
    var provinceNames : [String] = []
    var provinceCodes : [String] = []
    var distritNames : [String] = []
    var distritCodes : [String] = []
  
     var dayDatePickerVigencia = MonthYearPickerView()
    //variable que guarda la fecha de nacimiento seleccionada
    var currentDate:String = ""
    
    //variable que guarda la fecha de expiracion seleccionada
    var expirationDateSelected = ""
    
    //referencia de elementos de pantalla
    @IBOutlet weak var vigenciaTextField: UITextField!
    @IBOutlet weak var generoButton: UIButton!
    @IBOutlet weak var numeroTarjetaTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var fatherLastNameTextField: UITextField!
    @IBOutlet weak var motherLastNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var dniTextField: UITextField!
    @IBOutlet weak var digitoTextField: UITextField!
    @IBOutlet weak var addressTextField: UITextField!
    
      @IBOutlet weak var departamentButton: UIButton!
      @IBOutlet weak var provinceButton: UIButton!
      @IBOutlet weak var distritButton: UIButton!
    
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var laboralCenterTextField: UITextField!
    @IBOutlet weak var institucionPublicaTextField: UITextField!
    @IBOutlet weak var cargoPublicoTextField: UITextField!
    @IBOutlet weak var ocupationTextField: UITextField!
    
    @IBOutlet weak var hideConstraint: NSLayoutConstraint!
    @IBOutlet weak var showConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var institucionPublicaTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var cargoPublicoTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var checkPublicView: UIView!
    
    let checkboxPublic = M13Checkbox(frame: CGRect(x: 0.0, y: 0.0, width: 30.0, height: 30.0))

    
    let services = Services()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        self.services.delegate = self
        
        
//check
          self.checkboxPublic.boxType = .square
        self.checkboxPublic.tintColor = #colorLiteral(red: 0.948255837, green: 0.5015385747, blue: 0, alpha: 1)
         self.checkPublicView.addSubview(checkboxPublic)

          self.checkboxPublic.addTarget(self, action: #selector(checkBoxState), for: .valueChanged)
        
        // Date picker
        let toolDateBarVigencia = UIToolbar()
        toolDateBarVigencia.sizeToFit()
        let doneDateBtnVigencia = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(setDateVigencia(_:)))
        toolDateBarVigencia.setItems([doneDateBtnVigencia], animated: false)
        
        
        
        self.vigenciaTextField.inputAccessoryView = toolDateBarVigencia
        self.vigenciaTextField.inputView = self.dayDatePickerVigencia
        
        // Date picker
        let toolDateBar = UIToolbar()
        toolDateBar.sizeToFit()
        let doneDateBtn = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(setDate(_:)))
        toolDateBar.setItems([doneDateBtn], animated: false)
        
        self.dayDatePicker = UIDatePicker()
        let loc = Locale(identifier: "es")
        self.dayDatePicker.locale = loc
        self.dayDatePicker.datePickerMode = .date
        self.dayTextField.inputAccessoryView = toolDateBar
        self.dayTextField.inputView = self.dayDatePicker
        
        
        
        
        
    }
    
  
    
    //metodo que se ejecuta al presionar le boton de check de servidor publico (sirve para habilitar y deshabilitar el textField de descripción
      @objc func checkBoxState() {
          
       
          
          if self.checkboxPublic.checkState.rawValue == "Unchecked"{
              
         UIView.animate(withDuration: 1.0, delay: 1.2, options: .transitionCurlUp, animations: {
                self.showConstraint.priority = UILayoutPriority(rawValue: 800)
                               self.hideConstraint.priority = UILayoutPriority(rawValue: 900)
                
                     self.cargoPublicoTopConstraint.constant = 0
                     self.institucionPublicaTopConstraint.constant = 0
                     self.institucionPublicaTextField.text = ""
                     self.cargoPublicoTextField.text = ""
                     self.institucionPublicaTextField.isUserInteractionEnabled = false
                     self.cargoPublicoTextField.isUserInteractionEnabled = false
                     
            })
            
            
              
          }else{
            
            
            UIView.animate(withDuration: 1.0, delay: 1.2, options: .transitionCurlDown, animations: {
            self.showConstraint.priority = UILayoutPriority(rawValue: 900)
                                 self.hideConstraint.priority = UILayoutPriority(rawValue: 800)
              
            self.cargoPublicoTopConstraint.constant = 18
            self.institucionPublicaTopConstraint.constant = 18
            self.institucionPublicaTextField.isUserInteractionEnabled = true
            self.cargoPublicoTextField.isUserInteractionEnabled = true
             
            })
            
          }
          
      }
    
    
    @IBAction func backAction(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    

    //metodo que le da el formato a la fecha de vigencia tarjeta
    @objc func setDateVigencia(_ sender: UIButton){
        
        
        let yearString = String(self.dayDatePickerVigencia.year)
        let arrayYear = Array(yearString)
        
        let yearSend = "\(arrayYear[2])\(arrayYear[3])"
        var monthSend : String = "1"
        monthSend = String(self.dayDatePickerVigencia.month)
        switch self.dayDatePickerVigencia.month{
            
        case 1:
            monthSend = "01"
            break
        case 2:
            monthSend = "02"
            break
        case 3:
            monthSend = "03"
            break
        case 4:
            monthSend = "04"
            break
        case 5:
            monthSend = "05"
            break
        case 6:
            monthSend = "06"
            break
        case 7:
            monthSend = "07"
            break
        case 8:
            monthSend = "08"
            break
        case 9:
            monthSend = "09"
            break
            
            
        default:
            break
        }
        
        self.expirationDateSelected = "\(monthSend)/\(yearSend)"
        
        self.vigenciaTextField.text = "\(monthSend)/\(yearSend)"
        
        self.view.endEditing(true)
        
        
        
    }
    
    
    //metodo que le da el formato a la fecha
    @objc func setDate(_ sender: UIButton){
        let newDateFormat = DateFormatter()
        newDateFormat.dateStyle = .medium
        newDateFormat.timeStyle = .none
        newDateFormat.dateFormat = "dd/MM/yyyy"
        
        self.dayTextField.text = "\(newDateFormat.string(from: dayDatePicker.date))"
        
        
        let newDate = newDateFormat.date(from: newDateFormat.string(from: dayDatePicker.date))
        newDateFormat.dateFormat = "dd/MM/yyyy"
        currentDate = newDateFormat.string(from: newDate!)
        print("esta es \(currentDate)")
        
        self.view.endEditing(true)
        
    }
    
 //metodo que se ejecuta al presionar el botón de seleccionar genero
    @IBAction func generoSelectedAction(_ sender: UIButton) {
        
        
        
        ActionSheetStringPicker.show(withTitle: "Género", rows: self.generoNombres, initialSelection: 0, doneBlock: { (picker, value, selection) in
            
            
            let select = selection as! String
            
            
            self.generoButton.setTitleColor(#colorLiteral(red: 0.9254901961, green: 0.4196078431, blue: 0, alpha: 1), for: .normal)
            self.generoButton.setTitle(select, for: .normal)
            
            
            self.generoSelected = self.generoIds[value]
            
            
        }, cancel: { (action) in
            return
        }, origin: self.generoButton)
        
    }
    
    
    
    //metodo que se ejecuta al presionar el botón de enrolar
    @IBAction func enrolarAction(_ sender: UIButton) {
        
        if self.numeroTarjetaTextField.text!.count > 16 || self.numeroTarjetaTextField.text!.count < 16{
            
            self.alert(title: "¡Aviso!", message: "Debes agregar un numero de tarjeta valido para continuar.", cancel: "OK")
            return;
            
        }
        
        if self.nameTextField.text!.count < 3{
            
            self.alert(title: "¡Aviso!", message: "Debes agregar un nombre valido para continuar.", cancel: "OK")
            return;
            
        }
        
        if self.fatherLastNameTextField.text!.count < 3{
            
            self.alert(title: "¡Aviso!", message: "Debes agregar un apellido paterno valido para continuar.", cancel: "OK")
            return;
            
        }
        
        if self.motherLastNameTextField.text!.count < 3{
            
            self.alert(title: "¡Aviso!", message: "Debes agregar un apellido materno valido para continuar.", cancel: "OK")
            return;
            
        }
        
        if self.emailTextField.text!.count < 3{
            
            self.alert(title: "¡Aviso!", message: "Debes agregar un correo eléctronico valido para continuar.", cancel: "OK")
            return;
            
        }
        
        
        if self.dniTextField.text!.count > 8 || self.dniTextField.text!.count < 8{
            
            self.alert(title: "¡Aviso!", message: "Debes agregar un dni valido para continuar.", cancel: "OK")
            return;
            
        }
        
        if self.digitoTextField.text!.count > 1 || self.digitoTextField.text!.count < 1{
            
            self.alert(title: "¡Aviso!", message: "Debes agregar un dígito verificador valido para continuar.", cancel: "OK")
            return;
            
        }
        
        
        
        
        
        if self.currentDate == ""{
            self.alert(title: "¡Aviso!", message: "Debes seleccionar una fecha de nacimiento para continuar.", cancel: "OK")
            return;
        }
        
        if self.generoSelected == ""{
            
            self.alert(title: "¡Aviso!", message: "Debes seleccionar un género para continuar.", cancel: "OK")
            return;
            
        }
        
        if self.expirationDateSelected == ""{
            
            self.alert(title: "¡Aviso!", message: "Debes agregar una vigencia a tu tarjeta para continuar.", cancel: "OK")
            return;
            
        }
        
        if self.addressTextField.text!.count < 3{
                  
                  self.alert(title: "¡Aviso!", message: "Debes agregar una dirección valida para continuar.", cancel: "OK")
                  return;
                  
              }
        
          if self.departamentCode == ""{
                self.alert(title: "¡Aviso!", message: "Debes seleccionar un departamento para continuar.", cancel: "OK")
                return;
        }
          if self.provinceCode == ""{
                self.alert(title: "¡Aviso!", message: "Debes seleccionar una provincia para continuar.", cancel: "OK")
                return;
        }
          if self.distritCode == ""{
                self.alert(title: "¡Aviso!", message: "Debes seleccionar un distrito para continuar.", cancel: "OK")
                return;
        }
              
        
        if (self.phoneNumberTextField.text?.count)! < 9 || (self.phoneNumberTextField.text?.count)! > 12{
                alert(title: "¡Aviso!", message: "Agrega un número celular valido para continuar.", cancel: "OK");
                return;
            }
        
        if (self.laboralCenterTextField.text?.count)! < 4 {
                   alert(title: "¡Aviso!", message: "Agrega un centro laboral valido para continuar.", cancel: "OK");
                   return;
               }
        
        if (self.ocupationTextField.text?.count)! < 3 {
                 alert(title: "¡Aviso!", message: "Agrega una ocupación valida para continuar.", cancel: "OK");
                 return;
             }
        
        
        
        
        
        
        let decrypter = pootEngine()
        let panEncript = decrypter.encryptJSONString(self.numeroTarjetaTextField.text, withPassword: nil)
        
        let vigenciaEncript = decrypter.encryptJSONString(self.expirationDateSelected, withPassword: nil)
        
        var params  = ["email":self.emailTextField.text!,"dni":self.dniTextField.text!,"nombres":self.nameTextField.text!,"apellidoP":self.fatherLastNameTextField.text!,"apellidoM":self.motherLastNameTextField.text!,"digitoVerificador":self.digitoTextField.text!,"fechaNac":self.currentDate,"genero":self.generoSelected,"pan":panEncript,"vigencia":vigenciaEncript,"direccion":self.addressTextField.text!,
                       "regionId":self.departamentCode,
                       "locationId":self.provinceCode,
                       "subLocationId":self.distritCode,
                       "telefono":self.phoneNumberTextField.text!,
                       "workCenter":self.laboralCenterTextField.text!,
                       "occupation":self.ocupationTextField.text!]
        
        
        if self.checkboxPublic.checkState.rawValue == "Unchecked"{
          
              params["civilServant"] = "0"
        }else{
            //si es servidor publico
            //validaciones de los otros campos
            
            
            if (self.institucionPublicaTextField.text?.count)! < 3 {
                     alert(title: "¡Aviso!", message: "Agrega una institución pública valida para continuar.", cancel: "OK");
                     return;
                 }
            
            
            if (self.cargoPublicoTextField.text?.count)! < 3 {
                     alert(title: "¡Aviso!", message: "Agrega un cargo público valido para continuar.", cancel: "OK");
                     return;
                 }
            
            
            //se agregan parametros
                  params["civilServant"] = "1"
            params["publicInstitution"] = self.institucionPublicaTextField.text!
            params["publicOffice"] = self.cargoPublicoTextField.text!
            
        }
        
    
        print("PARAMS: \(params)")
        let headers  : HTTPHeaders = ["Content-Type":"application/json","Authorization":"Basic TWIxbDNDNHJkbVg6MUxvMGZRI3hHVHN1MXojNFs0TCo="]
        
        
        self.services.send(type: .POST, url: "TPP/5/4/es/operador/\(DataUser.USER_ID!)/enrollCard", params: params as [String : Any], header: headers, message: "Guardando",animate: false)
        
    
    }
    
    

     //metodo que se ejecuta al presionar departamentos
     @IBAction func departamentAction(_ sender: UIButton) {
         
         
         let headers  : HTTPHeaders = [:]
         self.services.send(type: .GET, url: "Catalogos/1/4/es/getDepartamentos", params: [:], header: headers, message: "Obteniendo",animate: true)
         
     }
     
     
     
     
    //metodo que se ejecuta al presioanr provincia
     @IBAction func provinceAction(_ sender: UIButton) {
         if self.departamentCode == ""{
             self.alert(title: "¡Aviso!", message: "Debes seleccionar un departamento para continuar.", cancel: "OK")
             return;
         }
         
         
         
         let headers  : HTTPHeaders = [:]
         self.services.send(type: .GET, url: "Catalogos/1/4/es/getProvincias?codigo=\(self.departamentCode)", params: [:], header: headers, message: "Obteniendo",animate: true)
         
         
     }
     
     //metodo que se ejecuta al presionar distrito
     @IBAction func distritAction(_ sender: UIButton) {
         if self.provinceCode == ""{
             self.alert(title: "¡Aviso!", message: "Debes seleccionar una provincia para continuar.", cancel: "OK")
             return;
     }
         
         
         
         let headers  : HTTPHeaders = [:]
         self.services.send(type: .GET, url: "Catalogos/1/4/es/getDistritos?codigo=\(self.provinceCode)", params: [:], header: headers, message: "Obteniendo",animate: true)
         
        
         
         
     }
    
 
    
    //metodos delegados de los servicios
    
    
    func responseService(type: ServicesTypes, endpoint: String, response: NSDictionary) {
        
        print("RESPONSE: \(response)")
        
        if endpoint == "TPP/5/4/es/operador/\(DataUser.USER_ID!)/enrollCard"{
            
            let idError = response["idError"] as! Int
            
         
            
            if idError == 0{
                
                let mensaje = response["mensajeError"] as! String
                
                let cards = response["cards"] as! NSArray
                let dictio = cards[0] as! NSDictionary
                let fechaAlta = dictio["fechaAlta"] as! String
                let idCard = String(dictio["id"] as! Int)
                let nombre = dictio["nombre"] as! String
                let pan = dictio["pan"] as! String
                
                let decrypter = pootEngine()
                let panDecript = decrypter.mcDecryptString(pan, withSensitive: false)
                
                let arrayChars = Array(panDecript!)
                
                let panFinish = "\(arrayChars[0])\(arrayChars[1])\(arrayChars[2])\(arrayChars[3])-****-****-\(arrayChars[arrayChars.count-4])\(arrayChars[arrayChars.count-3])\(arrayChars[arrayChars.count-2])\(arrayChars[arrayChars.count-1])"
                
                let objectTemp = CardModel(id: idCard, nombre: nombre, pan: panFinish, fechaAlta: fechaAlta)
                
                let vc = ResultViewController()
                vc.mensaje = mensaje
                vc.status = true
                vc.card = objectTemp
                self.navigationController?.pushViewController(vc, animated: true)
                
            }else{
                 let mensaje = response["mensajeError"] as! String
                let vc = ResultViewController()
                let objectTemp = CardModel(id: "", nombre: "", pan: "", fechaAlta: "")
                vc.mensaje = mensaje
                vc.status = false
                vc.card = objectTemp
                self.navigationController?.pushViewController(vc, animated: true)
                
                
            }
        }
        
    
        
        
        
    }
    
    func responseArrayService(type: ServicesTypes, endpoint: String, response: NSArray) {
        
        
        
        
        
               //departamentos
               if endpoint == "Catalogos/1/4/es/getDepartamentos"{
                   self.departamentCodes = []
                   self.departamentNames = []
           
                   for element in response{
                       
                       let dictio = element as! NSDictionary
                       
                       let departament = dictio["departamento"] as! String
                       let codigo = dictio["codigo"] as! String
                       
                       self.departamentNames.append(departament)
                       self.departamentCodes.append(codigo)
                   }
                   
                   //llamamos el picker
                   
                   
                   ActionSheetStringPicker.show(withTitle: "Departamentos", rows: self.departamentNames, initialSelection: 0, doneBlock: { (picker, value, selection) in
                       
                       
                       let select = selection as! String

                    self.departamentButton.setTitle(select, for: .normal)
                    self.departamentButton.setTitleColor(#colorLiteral(red: 0.9254901961, green: 0.4196078431, blue: 0, alpha: 1), for: .normal)
                    //   self.departamentTextField.text = select
                       
                       //si seleccionamos otro departamento borramos todas las provincias y distritos seleccionados
                       if self.departamentCode != self.departamentCodes[value]{
                           self.provinceCode = ""
                           self.distritCode  = ""
                        self.provinceButton.setTitle("Provincia", for: .normal)
                        self.provinceButton.setTitleColor(#colorLiteral(red: 0.8063663258, green: 0.8121158272, blue: 0.8293643314, alpha: 1), for: .normal)
                        self.distritButton.setTitle("Distrito", for: .normal)
                          self.distritButton.setTitleColor(#colorLiteral(red: 0.8063663258, green: 0.8121158272, blue: 0.8293643314, alpha: 1), for: .normal)
                           //self.provinceTextField.text = ""
                        
                           //self.distritTextField.text = ""
                       }
                       
                       self.departamentCode = self.departamentCodes[value]

                   }, cancel: { (action) in
                       return
                   }, origin: self.departamentButton)
                   
                   
               }
               
               
               
               //provincias
               if endpoint == "Catalogos/1/4/es/getProvincias?codigo=\(self.departamentCode)"{
                   self.provinceCodes = []
                   self.provinceNames = []
                   
                   for element in response{
                       
                       let dictio = element as! NSDictionary
                       
                       let province = dictio["provincia"] as! String
                       let codigo = dictio["codigo"] as! String
                       
                       self.provinceNames.append(province)
                       self.provinceCodes.append(codigo)
                   }
                   
                   //llamamos el picker
                   
                   
                   ActionSheetStringPicker.show(withTitle: "Provincia", rows: self.provinceNames, initialSelection: 0, doneBlock: { (picker, value, selection) in
                       
                       
                       let select = selection as! String
                       
                    self.provinceButton.setTitle(select, for: .normal)
                    self.provinceButton.setTitleColor(#colorLiteral(red: 0.9254901961, green: 0.4196078431, blue: 0, alpha: 1), for: .normal)
                     // self.provinceTextField.text = select
                       
                       //si seleccionamos otra provincia borramos los distritos seleccionados
                       if self.provinceCode != self.provinceCodes[value]{
                           self.distritCode  = ""
                          
                         self.distritButton.setTitle("Distrito", for: .normal)
                        self.distritButton.setTitleColor(#colorLiteral(red: 0.8063663258, green: 0.8121158272, blue: 0.8293643314, alpha: 1), for: .normal)
                        //self.distritTextField.text = ""
                       }
                       
                       self.provinceCode = self.provinceCodes[value]
                       
                   }, cancel: { (action) in
                       return
                   }, origin: self.provinceButton)
                   
                   
               }
               
               
               
               //distritos
               if endpoint == "Catalogos/1/4/es/getDistritos?codigo=\(self.provinceCode)"{
                   self.distritCodes = []
                   self.distritNames = []
                   
                   for element in response{
                       
                       let dictio = element as! NSDictionary
                       
                       let distrit = dictio["distrito"] as! String
                       let codigo = dictio["codigo"] as! String
                       
                       self.distritNames.append(distrit)
                       self.distritCodes.append(codigo)
                   }
                   
                   //llamamos el picker
                   
                   
                   ActionSheetStringPicker.show(withTitle: "Distrito", rows: self.distritNames, initialSelection: 0, doneBlock: { (picker, value, selection) in
                       
                       
                       let select = selection as! String
                       
                      self.distritButton.setTitle(select, for: .normal)
                    self.distritButton.setTitleColor(#colorLiteral(red: 0.9254901961, green: 0.4196078431, blue: 0, alpha: 1), for: .normal)
                    
                     //  self.distritTextField.text = select
                       
                    
                       
                       self.distritCode = self.distritCodes[value]
                       
                   }, cancel: { (action) in
                       return
                   }, origin: self.distritButton)
                   
                   
               }
        
        
    }
    
    func errorService(endpoint: String) {
       
            self.alert(title: "¡Aviso!", message: "No fue posible establecer conexión con el servidor.", cancel: "OK")
    }
    

   
}







