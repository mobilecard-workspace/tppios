

import Foundation
import UIKit
import Alamofire



protocol ServicesDelegate {
    func responseService(type:ServicesTypes,endpoint:String, response:NSDictionary)
    func responseArrayService(type:ServicesTypes,endpoint:String, response:NSArray)
    func errorService(endpoint:String)
}


//solo cuenta con servicios post actualmente
enum ServicesTypes {
    case GET
    case POST
    case POSTURLENCODED
    case PUT
    case DELETE
}



//objeto que almacena la informacion del servicio
class Services: NSObject{
     var manager: SessionManager!;
    //delegado
    var delegate: ServicesDelegate?
    
    //cuerpo del objeto
    var type : ServicesTypes?
    var endpoint : String?
    
    //url principal del servidor
    var urlServices :String! = "https://www.mobilecard.mx/"

   
 
    
    
    
  
    
    //metodo que ejecuta la consulta 
    func send(type: ServicesTypes,url: String,params:[String:Any],header:HTTPHeaders,message:String,animate : BooleanLiteralType){
     
        
        
        
        self.endpoint = url
        self.type = type
        
        
        
        if message != ""{
        DispatchQueue.main.async {
            
            
            
        var config:LoaderView.Config = LoaderView.Config()
        config.size = 100;
        config.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8);
        config.spinnerColor    = UIColor.white;
        config.titleTextColor  = UIColor.white;
        LoaderView.setConfig(config: config);
        
        LoaderView.show(title: message, animated: animate)
        }
            
        }

        
        let stringUrl = "\(urlServices!)\(url)"
        print("URL: \(stringUrl)")
       
        if type == .POST{
        
        Alamofire.request(stringUrl, method: .post, parameters: params, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            if(response.result.isSuccess){
               
                DispatchQueue.main.async {
                LoaderView.hide()
                }
                let dic = response.result.value  as? NSDictionary
                
                if dic != nil{
                    
                    //respondemos el servicio
                    self.delegate?.responseService(type: self.type!, endpoint: self.endpoint!, response: dic!)
                    
                }else{
                    
                    let dicArray = response.result.value  as! NSArray
                    
                   
                    //respondemos el servicio
                    self.delegate?.responseArrayService(type: self.type!, endpoint: self.endpoint!, response: dicArray)
                    
                }
                
            }else{
                //no se pudo conectar con el servicio
             
                if message != ""{
                DispatchQueue.main.async {
                    LoaderView.hide()
                }
                }
                self.delegate?.errorService(endpoint: self.endpoint!)
                print("error")
            }
        }
            
        }else  if type == .DELETE{
            
            Alamofire.request(stringUrl, method: .delete, parameters: params, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
                if(response.result.isSuccess){
                    
                    DispatchQueue.main.async {
                        LoaderView.hide()
                    }
                    let dic = response.result.value  as? NSDictionary
                    
                    if dic != nil{
                        
                        //respondemos el servicio
                        self.delegate?.responseService(type: self.type!, endpoint: self.endpoint!, response: dic!)
                        
                    }else{
                        
                        let dicArray = response.result.value  as! NSArray
                        
                        
                        //respondemos el servicio
                        self.delegate?.responseArrayService(type: self.type!, endpoint: self.endpoint!, response: dicArray)
                        
                    }
                    
                }else{
                    //no se pudo conectar con el servicio
                    
                    if message != ""{
                        DispatchQueue.main.async {
                            LoaderView.hide()
                        }
                    }
                    self.delegate?.errorService(endpoint: self.endpoint!)
                    print("error")
                }
            }
            
        }else if type == .POSTURLENCODED{
           
          
            Alamofire.request(stringUrl, method: .post, parameters: params, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
                if(response.result.isSuccess){
                    
                    if message != ""{
                    DispatchQueue.main.async {
                        LoaderView.hide()
                    }
                    }
                    let dic = response.result.value  as? NSDictionary
                    
                    if dic != nil{
                        
                        //respondemos el servicio
                        self.delegate?.responseService(type: self.type!, endpoint: self.endpoint!, response: dic!)
                        
                    }else{
                        
                        let dicArray = response.result.value  as! NSArray
                        
                        
                        //respondemos el servicio
                        self.delegate?.responseArrayService(type: self.type!, endpoint: self.endpoint!, response: dicArray)
                        
                    }
                    
                }else{
                    //no se pudo conectar con el servicio
                    
                    if message != ""{
                    DispatchQueue.main.async {
                        LoaderView.hide()
                    }
                    }
                    self.delegate?.errorService(endpoint: self.endpoint!)
                    print("error")
                }
            }
            
        }else    if type == .PUT{
            
            Alamofire.request(stringUrl, method: .put, parameters: params, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
                if(response.result.isSuccess){
                    
                    if message != ""{
                    DispatchQueue.main.async {
                            LoaderView.hide()
                    }
                    }
                    let dic = response.result.value  as? NSDictionary
                    
                    if dic != nil{
                        
                        //respondemos el servicio
                        self.delegate?.responseService(type: self.type!, endpoint: self.endpoint!, response: dic!)
                        
                    }else{
                        
                        let dicArray = response.result.value  as! NSArray
                        
                        
                        //respondemos el servicio
                        self.delegate?.responseArrayService(type: self.type!, endpoint: self.endpoint!, response: dicArray)
                        
                    }
                    
                }else{
                    //no se pudo conectar con el servicio
                    if message != ""{
                    DispatchQueue.main.async {
                        LoaderView.hide()
                    }
                    }
                    self.delegate?.errorService(endpoint: self.endpoint!)
                    print("error")
                }
            }
            
        }else if type == .GET{
            
            Alamofire.request(stringUrl, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
                if(response.result.isSuccess){
                    
                    let dic = response.result.value  as? NSDictionary
                        
                        if dic != nil{
                            if message != ""{
                            DispatchQueue.main.async { LoaderView.hide()}
                            }
                            //respondemos el servicio
                            self.delegate?.responseService(type: self.type!, endpoint: self.endpoint!, response: dic!)
                            
                        }else{
                          
                         let dicArray = response.result.value  as! NSArray
                         
                            if message != ""{
                            DispatchQueue.main.async { LoaderView.hide()}
                            }
                            //respondemos el servicio
                            self.delegate?.responseArrayService(type: self.type!, endpoint: self.endpoint!, response: dicArray)
                            
                    }
                        
                    
                    
                    
            
                    
                    
                }else{
                    //no se pudo conectar con el servicio
                    if message != ""{
                    DispatchQueue.main.async { LoaderView.hide()}
                    }
                    self.delegate?.errorService(endpoint: self.endpoint!)
                    print("error")
                }
            }
            
            
            
        }
        
        
        
        
        
        
        
        
        
        }
    
            
 
        
        
    
       
        
   
        

    
   
 
    
}





