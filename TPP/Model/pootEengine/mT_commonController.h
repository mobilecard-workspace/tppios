//
//  mT_commonController.h
//  Mobilecard
//
//  Created by David Poot on 10/2/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITextField+Validations.h"
#import "UIPickerView+Automated.h"
#import "UIDatePicker+Automated.h"
#import "pootEngine.h"
#import "MC_Colors.h"
#import "mT_appLinks.h"


#define idApplication 1
#define idApp 1

//#define colHeader @"44446AF1545606C32309AB2F6E2A33AB"
#define colHeader @"49a41439bcfb1faeecf6a498b055fc8e"



typedef enum
{
    viewTypeRecargas,
    viewTypeServicios
} viewType;

typedef enum
{
    walletViewTypeRegular,
    walletViewTypeSelection,
    walletViewTypeSelectionOnlyDebit,
} walletViewType;

typedef enum
{
    updateCardTypeNew,
    updateCardTypeExisting,
    updateCardTypeRegistry,
} updateCardType;

typedef enum
{
    serviceTypeRecharge=0,
    serviceTypeServices,
    serviceTypeDonations,
    serviceTypeCellPhoneTime=10,
    serviceTypeTAE,
    serviceTypeTelcel,
    serviceTypeMovistar,
    serviceTypeIusacell,
    serviceTypeUnefon,
    serviceTypeNextel,
    serviceTypeVirgin,
    serviceTypeTelcelGeneric,
    serviceTypeTolls=20,
    serviceTypeIAVE,
    serviceTypePASE,
    serviceTypeInterjet,
    serviceTypeVitamedica,
    serviceTypeVitamedicaIndividual,
    serviceTypeVitamedicaGroup,
    serviceTypeRedCross,
    serviceTypeMXTransfers,
    serviceTypeLaCuenta,
    serviceTypeCuantoTeDebo,
    serviceTypeCuantoTeDeboByID,
    serviceTypePaymentPE,
    serviceTypeMobileTag,
    serviceCardPressentMx
} serviceType;

typedef enum
{
    paymentTypeACI=0,
    paymentTypeViamericas,
} paymentType;

typedef enum
{
    acceptanceTypeTerms,
    acceptanceTypePrivacy,
} acceptanceType;

typedef enum
{
    favoriteTopUpTae=1,
    favoriteTopUpToll,
    favoriteTopUpUSA,
} favoriteType;
static NSString const *kIDApplication = @"idAplicacion";

//Login variables (not public)
static NSString const *kUserDetailsKey = @"userDetails";
static NSString const *kUserLoginKey = @"login";
static NSString const *kUserIDKey = @"ideUsuario";
static NSString const *kUserPWKey = @"PW";
static NSString const *kUserTempPWKey = @"tempPW";
static NSString const *kUserStatus = @"status";

static NSString const *kResultKey = @"resultado";
static NSString const *kMessageKey = @"mensaje";

static NSString const *kIdentifierKey = @"identifier";
static NSString const *kHeightKey = @"Height";

//public variables
static NSString const *kIDKey = @"id";
static NSString const *kKeyKey = @"clave";
static NSString const *kDescriptionKey = @"descripcion";
static NSString const *kDetailsKey = @"nombre";
static NSString const *kImgUser = @"img";//
static NSString const *kIDError = @"idError";
static NSString const *kErrorMessage = @"mensajeError";
static NSString const *kIDStatus = @"idUsrStatus";
static NSString const *kIDJumio = @"usr_jumio";
static NSString const *kIDJumioCommerce = @"jumioStatus";
static NSString const *kScanReferenceKey = @"scanReference";

static NSString const *kModuleKey = @"modulos";
static NSString const *kIDModuleKey = @"idModule";
static NSString const *kIdUserKey = @"idUser";
static NSString const *kIdNegocio = @"negocio";


//Registry variables
static NSString const *kUserType = @"userType";
static NSString const *kUserLogin = @"usrLogin";
static NSString const *kUserPassword = @"usrPwd";
static NSString const *kUserIdCountry = @"idPais";
static NSString const *kUserName = @"usrNombre";
static NSString const *kUserLastName = @"usrApellido";
static NSString const *kUserMotherLastName = @"usrMaterno";
static NSString const *kUserPhone = @"usrTelefono";
static NSString const *kUserEmail = @"eMail";
static NSString const *kUserGender = @"usrSexo";
static NSString const *kCardNumber = @"usrTdcNumero";
static NSString const *kCardValid = @"usrTdcVigencia";
static NSString const *kCardCVV = @"usrTdcCodigo";
static NSString const *kLanguage = @"idioma";
static NSString const *kCardIDType = @"idTipoTarjeta";
static NSString const *kUserSSN = @"usrNss";
static NSString const *kAmexZip = @"usrCp";
static NSString const *kAmexAddress = @"usrDomAmex";


//Commerce
static NSString const *keycomision = @"comisionPorcentaje";


//Wallet
static NSString const *kUserCardArray = @"tarjetas";
static NSString const *kUserCardTransactions = @"movements";

//Tags
static NSString const *kTagDVKey = @"dv";
static NSString const *kTagLabelKey = @"etiqueta";
static NSString const *kTagNumberKey = @"tag";
static NSString const *kTagTypeKey = @"tiporecargatag";
static NSString const *ktagUserKey = @"usuario";

//Viamericas
static NSString const *kUserSenderId = @"idSender";

//Favorites
static NSString const *kUserFavoritesKey = @"favorites";

//Menu
static NSString const *kUSALocation = @"usaLocation";

static pootEngine *decriptManager;

@interface mT_commonController : UIViewController <UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UIAlertViewDelegate, UIActionSheetDelegate, pootEngineDelegate>
{
    UIPickerView_Automated *picker;
    UIDatePicker_Automated *datePicker;
    
    UIAlertView *blocker;
}
- (void) showAlertWithTitle:(NSString *)title message:(NSString *)message;


//UITextFields
- (BOOL)textFieldShouldEndEditing:(UITextField_Validations *)textField;
- (BOOL)subTextFieldShouldEndEditing:(UITextField_Validations *)textField;
- (BOOL)textFieldShouldBeginEditing:(UITextField_Validations *)textField;
- (BOOL)subTextFieldShouldBeginEditing:(UITextField_Validations *)textField;

- (BOOL) validateFieldsInArray:(NSArray *)fieldArray;
- (BOOL) validateFieldsWithoutAlertInArray:(NSArray *)fieldArray;
+(NSString*) decryptHard:(NSString *) string;


//UIPickerView
- (void)initializePickersForView:(UIView*)view;
- (NSString *)subPickerView:(UIPickerView_Automated *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component;
- (void)subPickerView:(UIPickerView_Automated *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component;
- (void) subDatePickerValueChanged:(id)sender;
- (void) subDismissPickers;

//UITextFields Management
- (void) removeAllTextFields;
- (void) addValidationTextField:(UITextField_Validations*)textField;
- (NSMutableArray*)getValidationTextFields;
- (void)addValidationTextFieldsToDelegate;
- (BOOL)subTextField:(UITextField_Validations *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;

//Misc
- (NSMutableDictionary*)cleanDictionary:(NSMutableDictionary*)resp;
- (void)addShadowToView:(UIView*)view;
- (UIImage *)imageFromColor:(UIColor *)color;
- (void) lockViewWithMessage:(NSString*)message;
- (void) unLockView;

- (NSString *) platformString;



//ONLY HERE
- (void) initializeBlockerArray;

- (void)dismissAllTextFields;

@end

@interface NSData (Poot)

+ (NSData *)sha512:(NSData *)data;

@end


