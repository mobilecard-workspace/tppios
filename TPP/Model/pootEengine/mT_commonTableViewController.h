//
//  mT_commonTableViewController.h
//  Mobilecard
//
//  Created by David Poot on 10/25/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITextField+Validations.h"
#import "UIPickerView+Automated.h"
#import "UIDatePicker+Automated.h"
#import "pootEngine.h"
#import "MC_Colors.h"
#import "mT_appLinks.h"


#import "mT_commonController.h"

@interface mT_commonTableViewController : UITableViewController <UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UIAlertViewDelegate, UIActionSheetDelegate, pootEngineDelegate>
{
    UIPickerView_Automated *picker;
    UIDatePicker_Automated *datePicker;
    
    UIAlertView *blocker;
}

//UITextFields
- (BOOL)textFieldShouldEndEditing:(UITextField_Validations *)textField;
- (BOOL)subTextFieldShouldEndEditing:(UITextField_Validations *)textField;
- (BOOL)textFieldShouldBeginEditing:(UITextField_Validations *)textField;
- (BOOL)subTextFieldShouldBeginEditing:(UITextField_Validations *)textField;

- (BOOL) validateFieldsInArray:(NSArray *)fieldArray;
- (BOOL) validateFieldsWithoutAlertInArray:(NSArray *)fieldArray;

//UIPickerView
- (void)initializePickersForView:(UIView*)view;
- (NSString *)subPickerView:(UIPickerView_Automated *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component;
- (void)subPickerView:(UIPickerView_Automated *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component;
- (void) subDatePickerValueChanged:(id)sender;
- (void) subDismissPickers;

//UITextFields Management
- (void) removeAllTextFields;
- (void) addValidationTextField:(UITextField_Validations*)textField;
- (NSMutableArray*)getValidationTextFields;
- (void)addValidationTextFieldsToDelegate;
- (BOOL) subTextFieldShouldBeginEditing:(UITextField_Validations *)textField;
- (BOOL)subTextField:(UITextField_Validations *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;

//Misc
- (NSMutableDictionary*)cleanDictionary:(NSMutableDictionary*)resp;
- (void)addShadowToView:(UIView*)view;
- (UIImage *)imageFromColor:(UIColor *)color;
- (void) lockViewWithMessage:(NSString*)message;
- (void) unLockView;

- (NSString *) platformString;

- (void)dismissAllTextFields;

- (NSString*) encryptString:(NSString*)string;

- (NSString*) maskCardStringWithString:(NSString*)string;

@end
