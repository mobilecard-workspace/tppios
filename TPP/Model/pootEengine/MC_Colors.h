//
//  MC_Colors.h
//  Mobilecard
//
//  Created by David Poot on 10/26/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (MC_Colors)

+ (UIColor*) lightMCGreyColor;
+ (UIColor*) MCGreyCyanColor;
+ (UIColor*) darkMCColor;
+ (UIColor*) darkMCGreyColor;
+ (UIColor*) darkMCOrangeColor;
+ (UIColor*) lightMCOrangeColor;
+ (UIColor*) MCRedColor;
+ (UIColor*) MCGreenColor;
//+ (UIColor *) colorFromHexCode:(NSString *)hexString;


@end
