//
//  UIPickerView+Automated.h
//  mT
//
//  Created by David Poot on 11/28/13.
//  Copyright (c) 2013 addcel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITextField+Validations.h"

@interface UIPickerView_Automated : UIPickerView

@property (nonatomic, strong) UITextField_Validations *textField;


@end
