//
//  MC_Colors.m
//  Mobilecard
//
//  Created by David Poot on 10/26/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "MC_Colors.h"

@implementation UIColor (MC_Colors)

+ (UIColor*)lightMCGreyColor
{
    return  [self colorFromHexCode:@"A2A3A4"];
   // return [UIColor colorWithRed:240.0/255.0 green:239.0/255.0 blue:235.0/255.0 alpha:1];
}

+ (UIColor*) MCGreyCyanColor
{
     return  [self colorFromHexCode:@"009FE3"];
   // return [UIColor colorWithRed:168.0/255.0 green:171.0/255.0 blue:170.0/255.0 alpha:1];
}

+ (UIColor *)darkMCColor
{
    return  [self colorFromHexCode:@"000000"];
}
+ (UIColor*) darkMCGreyColor{
    return [UIColor colorWithRed:97.0/255.0 green:99.0/255.0 blue:96.0/255.0 alpha:1];
}
+ (UIColor *)darkMCOrangeColor
{
    return  [self colorFromHexCode:@"E96B00"];
  //  return [UIColor colorWithRed:229.0/255.0 green:80.0/255.0 blue:46.0/255.0 alpha:1];
}

+ (UIColor *)lightMCOrangeColor
{
    return  [self colorFromHexCode:@"F6B63E"];
   // return [UIColor colorWithRed:238.0/255.0 green:119.0/255.0 blue:0.0/255.0 alpha:1];
}
+ (UIColor*) MCRedColor{ 
    return  [self colorFromHexCode:@"E0040B"];
}
+ (UIColor*) MCGreenColor{
     return  [self colorFromHexCode:@"98BF13"];
}
+ (UIColor *) colorFromHexCode:(NSString *)hexString {
    NSString *cleanString = [hexString stringByReplacingOccurrencesOfString:@"#" withString:@""];
    if([cleanString length] == 3) {
        cleanString = [NSString stringWithFormat:@"%@%@%@%@%@%@",
                       [cleanString substringWithRange:NSMakeRange(0, 1)],[cleanString substringWithRange:NSMakeRange(0, 1)],
                       [cleanString substringWithRange:NSMakeRange(1, 1)],[cleanString substringWithRange:NSMakeRange(1, 1)],
                       [cleanString substringWithRange:NSMakeRange(2, 1)],[cleanString substringWithRange:NSMakeRange(2, 1)]];
    }
    if([cleanString length] == 6) {
        cleanString = [cleanString stringByAppendingString:@"ff"];
    }
    
    unsigned int baseValue;
    [[NSScanner scannerWithString:cleanString] scanHexInt:&baseValue];
    
    float red = ((baseValue >> 24) & 0xFF)/255.0f;
    float green = ((baseValue >> 16) & 0xFF)/255.0f;
    float blue = ((baseValue >> 8) & 0xFF)/255.0f;
    float alpha = ((baseValue >> 0) & 0xFF)/255.0f;
    
    return [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
}
@end
