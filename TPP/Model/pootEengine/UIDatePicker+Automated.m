//
//  UIDatePicker+Automated.m
//  mT
//
//  Created by David Poot on 11/28/13.
//  Copyright (c) 2013 addcel. All rights reserved.
//

#import "UIDatePicker+Automated.h"

@implementation UIDatePicker_Automated

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end