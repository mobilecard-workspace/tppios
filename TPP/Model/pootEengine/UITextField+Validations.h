//
//  UITextField+Validations.h
//  mT
//
//  Created by David Poot on 11/27/13.
//  Copyright (c) 2013 addcel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JVFloatLabeledTextField.h"


typedef enum {
    mTErrorCodeEmptyRequired            = 0x00001,
    mTErrorCodeNonProperLength          = 0x00002,
    mTErrorCodeInvalidPasswordLength    = 0x00004,
    mTErrorCodeInvalidLoginLength       = 0x00008,
    mTErrorCodeInvalidCardLength        = 0x00010,
    mTErrorCodeInvalidEmail             = 0x00020,
    mTErrorCodeInvalidComboSelection    = 0x00040,
    mtErrorCodeInvalidIAVELength        = 0x00080,
    mTErrorCodeInvalidPASELength        = 0x00100,
    mTErrorCodeInvalidLongIAVELength    = 0x00200,
    mTErrorCodeInvalidLongPASELength    = 0x00400,
    mTErrorCodeInvalidCharacterSet      = 0x00800,
    mTErrorCodeInvalidRFC               = 0x01000,
} mTErrorCode;

typedef enum {
    textFieldTypeUserName,
    textFieldTypePassword,
    textFieldTypeCvv2,
    textFieldTypeValidCard,
    textFieldTypeCellphone,
    textFieldTypeEmail,
    textFieldTypeName,
    textFieldTypeLastName,
    textFieldTypeMotherLastName,
    textFieldTypeBirthdate,
    textFieldTypeGender,
    textFieldTypeHomePhone,
    textFieldTypeState,
    textFieldTypeCity,
    textFieldTypeStreet,
    textFieldTypeExtNum,
    textFieldTypeIntNum,
    textFieldTypeTown,
    textFieldTypeZip,
    textFieldTypeCard,
    textFieldTypeAddress,
    textFieldTypeSelector,
    textFieldTypeDate,
    textFieldTypeDateBeforeNow,
    textFieldTypeAddressAMEX,
    textFieldTypeZipAMEX,
    textFieldTypedateAndTime,
    textFieldTypeRFC,
    
    textFieldTypeRequiredCombo,
    textFieldTypeGeneralNoRequired,
    textFieldTypeGeneralRequired,
    textFieldTypeGeneralNumericNoRequired,
    textFieldTypeGeneralNumericRequired,
    
    textFieldTypeTagNameTag,
    textFieldTypeIAVETag,
    textFieldTypeIAVEDV,
    textFieldTypePASETag,
    textFieldTypePASEDV,
} textFieldType;

//IB_DESIGNABLE
@interface UITextField_Validations: JVFloatLabeledTextField

@property (nonatomic, assign) BOOL required;
@property BOOL active;

@property (retain, nonatomic) UIColor *invalidColor;
@property (retain, nonatomic) UIColor *validColor;
@property (retain, nonatomic) UIColor *underlineColor;

@property (nonatomic, assign) textFieldType type;
@property int maxLength;
@property int minLength;
@property (nonatomic, strong) NSCharacterSet *allowedCharacters;
@property (nonatomic, strong) NSString *hiddenString;
@property (strong, nonatomic) NSString *helpString;

@property int selectedID;
@property int selectedID2;
@property (nonatomic, strong) NSString *idLabel;
@property (nonatomic, strong) NSString *descriptionLabel;

@property (nonatomic, strong) NSArray *infoArray;
@property (nonatomic, strong) NSArray *infoArray2;

@property (strong, nonatomic) NSDate *minDate;
@property (strong, nonatomic) NSDate *maxDate;

@property (strong, nonatomic) NSCharacterSet *customCharSet;

@property int textFieldState; //0 setUnderlineActiveColor, 1 validation OK, 2 validation NOK

@property BOOL blackInWhite;

@property (strong, nonatomic) UIView *underline;
@property BOOL allowEdition;



- (void) getSelectedIDWithID:(int)ID;
- (void) setUpTextFieldAs:(textFieldType)tfType;
- (NSInteger) validateField:(NSInteger)error;
- (void) disable;
- (void) enable;
- (void) changeRequiredStatusTo:(BOOL)newRequiredStatus;
- (void)updateValidation;

@end
