//
//  MC_TermsAndConditionsCtrller.h
//  mobilecard
//
//  Created by David Poot on 9/30/12.
//  Copyright (c) 2012 Addcel. All rights reserved.
//

#import "mT_commonController.h"

@protocol termsDelegate <NSObject>

@required
-  (void) termsAcceptance:(BOOL) result;

@end

@interface MC_TermsAndConditionsCtrller : mT_commonController <pootEngineDelegate, UIAlertViewDelegate>
{
    pootEngine *termsManager;
}

- (IBAction)agreeAction:(id)sender;
- (IBAction)noAgreeAction:(id)sender;

@property (nonatomic, assign) acceptanceType type;
@property (unsafe_unretained, nonatomic) IBOutlet UITextView *termsText;
@property (nonatomic, assign) id <termsDelegate, NSObject> delegate;
@property (unsafe_unretained, nonatomic) IBOutlet UIBarButtonItem *agreeButton;
@property (unsafe_unretained, nonatomic) IBOutlet UIBarButtonItem *noAgreeButton;

@end
