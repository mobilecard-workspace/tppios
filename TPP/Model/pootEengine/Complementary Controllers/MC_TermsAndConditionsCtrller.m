//
//  MC_TermsAndConditionsCtrller.m
//  mobilecard
//
//  Created by David Poot on 9/30/12.
//  Copyright (c) 2012 Addcel. All rights reserved.
//

#import "MC_TermsAndConditionsCtrller.h"

@interface MC_TermsAndConditionsCtrller ()

@end

@implementation MC_TermsAndConditionsCtrller

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (UIStatusBarAnimation)preferredStatusBarUpdateAnimation {
    return UIStatusBarAnimationFade;
}

- (void)viewWillAppear:(BOOL)animated
{
    //[self addShadowToView:self.navigationController.view];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    [_termsText setText:@""];
    [super viewWillAppear:YES];
}

- (void)viewDidAppear:(BOOL)animated
{
    //[self.navigationController setNavigationBarHidden:YES animated:YES];
    
    !termsManager?termsManager = [[pootEngine alloc]init]:nil;
    [termsManager setDelegate:self];
    [termsManager setShowComments:developing];
    
    NSMutableDictionary *header = [[NSMutableDictionary alloc] init];
    
    [header setObject:[termsManager convertToSecureLevel2:@"mobilecardios"] forKey:@"client"];
    
    NSString *typeOfAcceptance = @"terminos/";
    
    switch (_type) {
        case acceptanceTypeTerms:
        {
            typeOfAcceptance = @"terminos/";
        }
            break;
            
        case acceptanceTypePrivacy:
        {
            typeOfAcceptance = @"privacidad/";
            [_noAgreeButton setTitle:@""];
            [_agreeButton setTitle:@"OK"];
        }
            break;
        default:
            break;
    }
    
    [termsManager startJSONRequestWithURL:[NSString stringWithFormat:@"%@%@%@/%@", userManagementURL, typeOfAcceptance, @"1", NSLocalizedString(@"lang", nil)] withPost:nil andHeader:header];
    
    [self lockViewWithMessage:nil];
    [_agreeButton setEnabled:NO];
    [_noAgreeButton setEnabled:NO];
    
    [self setNeedsStatusBarAppearanceUpdate];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillUnload {
    [self setTermsText:nil];
    [self setAgreeButton:nil];
    [self setNoAgreeButton:nil];
    [super viewDidUnload];
}

-(void)dealloc
{
    [termsManager setDelegate:nil];
}

- (IBAction)agreeAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        if ([_delegate conformsToProtocol:@protocol(termsDelegate)]&&[_delegate respondsToSelector:@selector(termsAcceptance:)]) {
            [_delegate termsAcceptance:YES];
        }
    }];
}

- (IBAction)noAgreeAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        if ([_delegate conformsToProtocol:@protocol(termsDelegate)]&&[_delegate respondsToSelector:@selector(termsAcceptance:)]) {
            [_delegate termsAcceptance:NO];
        }
    }];
}




//pootEngine Delegates
- (void)managerSuccess:(pootEngine *)manager jsonParsed:(NSArray *)json
{
    [self unLockView];
    [_agreeButton setEnabled:YES];
    [_noAgreeButton setEnabled:YES];
    
    NSDictionary *response = (NSDictionary*)json;
    
    if ([response[kIDError] intValue]==0) {
        [_termsText setText:response[@"termino"]];
    } else {
        UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertMsg show];
        
        [self noAgreeAction:nil];
    }
    /*
    NSDictionary *preResp = [NSDictionary dictionaryWithObject:json forKey:kResultKey];
    NSDictionary *resp = [preResp objectForKey:kResultKey];
    
    [_termsText setText:[resp objectForKey:@"Descripcion"]];
    */
     
    [_termsText setEditable:NO];
    [_termsText setSelectable:NO];
}

@end
