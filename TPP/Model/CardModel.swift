

import Foundation
import UIKit

class CardModel : NSObject{
    
    
    
    var id : String
    var nombre: String
    var pan : String
    var fechaAlta: String
    
    
    
    
    
    init(id : String , nombre: String , pan : String , fechaAlta:String) {
        
        self.id = id
        self.nombre = nombre
        self.pan = pan
        self.fechaAlta = fechaAlta
        
    }
    
}
