

import Foundation
import UIKit

class HistoryModel : NSObject{
    
    
    
    var nombre :String!
    var fecha: String!
    var tarjeta : String!
    var monto : String!
    var type : String! = "alta" //alto o recarga
    
    
    
    
    init(nombre : String , fecha : String , tarjeta: String , monto : String , type: String) {
        
        self.nombre = nombre
        self.fecha = fecha
        self.tarjeta = tarjeta
        self.monto = monto
        self.type = type
        
    }
    
}
